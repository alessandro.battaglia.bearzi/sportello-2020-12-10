<?php

$var = 4;

$var = "parola";

// Definizione di array
$var = [
    "valore1",
    "valore2",
    "valore3",
    "valore4",
    "valore5",
];

// Definizione di array
$var = array(
    "valore1",
    "valore2",
    "valore3",
    "valore4",
    "valore5",
);

$var = [
    "valore1",
    3,
    4.6,
    // ...
];

$var = [
    0 => "valore1",
    1 => 3,
    2 => 4.6,
    // ...
];

$persona = [
    "nome" => "Pierino",
    "età" => 30,
    "altezza" => 146,
    // ...
];

$nome = $persona["nome"];
$nome = $persona['nome'];
$valore = $var[0];
