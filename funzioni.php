<?php

function somma($parametro1, $parametro2)
{
    return $parametro1 + $parametro2;
}

function sommaConTipo(int $parametro1, ?int $parametro2): int
{
    return $parametro1 + $parametro2;
}

function sommaConTipoAMeta($parametro1, int $parametro2)
{
    return $parametro1 + $parametro2;
}

function sommaConDefault($parametro1, int $parametro2 = 3)
{
    return $parametro1 + $parametro2;
}

sommaConDefault(6, 5);

/**
 * @param float|int $n1
 * @param float|int $n2
 * @return float|int
 */
function sommaCapisci($n1, $n2)
{
    if (is_int($n1) and is_int($n2)) {
        return sommaInt($n1, $n2);
    }

    return sommaFloat($n1, $n2);
}

function sommaInt(int $n1, int $n2): int
{
    return $n1 + $n2;
}

function sommaFloat(float $n1, float $n2): float
{
    return $n1 + $n2;
}

function stampaSomma($n1, $n2): void
{
    echo $n1 + $n2;
}

function lancioEccezione(string $message = null) {
    throw new \RuntimeException($message);
}
