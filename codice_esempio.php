<?php

$var = 10;

// Connessione al DATABASE

$colore = "bianco";
$nome = "nome";

?>

    <html lang="it">
    <head>
        <title></title>

        <?php if ($colore == "nero") { ?>
            <link href="..." rel="stylesheet">
        <?php } ?>
    </head>
    <body>

    ...

    <?php echo $nome; ?>

    ...

    <?= ($nome === "nome" ? "ok" : "no") ?>
    <?= $nome ?>

    ...

    <?php

    // Visualizzare contenuti

    ?>
    </body>
    </html>

<?php

// Chiudere la connessione al DB
