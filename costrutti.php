<?php

$valore = 4;
echo $valore; // Stampo 4
echo "$valore"; // Stampo 4
echo '$valore'; // Stampo $valore

$condizione = false;

if ($condizione) {
    // Se condizione è verificata
} else {
    // Se condizione non è verificata
}

$stringa = "sbagliato";

if ($stringa === "giusto") {
    // "string".equals("string2")
}

// == $variabile stesso valore
// === $variabile tipo indentico + $variabile stesso valore

if ($numero = sommaInt(4, 5)) {
    echo $numero;
}
if (2 == "2") { // vero

}
if (2 === "2") { // falso

}

// int === Integer
// float === Float
if (is_int($numero)) {

}

if ($stringa) {
    // se la stringa è vuota ==> true
    // se la stringa è null ==> false
    // se la stringa è 0 ==> false
}
if (empty($stringa)) {
    // se la stringa è vuota ==> false
    // se la stringa è null ==> false
    // se la stringa è 0 ==> false
}

if (isset($numeroDaControllare) && $numeroDaControllare === 4) {
    // Fai qualcosa
    echo $numeroDaControllare;
}

// $persona esiste, è scontato per noi
$persona = dammiPersona();
if (isset($persona["name"])) {
    echo $persona["name"];
}

if (isset($persona) && isset($persona["name"])) {
    echo $persona["name"];
}

// Come sempre
for ($i = 0; $i < 5; $i++) {
    // Fai qualcosa
}

$array = [
    "valore 1",
    "valore 2",
    "elementoImportante" => "valore 3",
];
foreach ($array as $valoreNelCirclo) {
    echo $valoreNelCirclo;
}

$persona = [
    "nome" => "Pierino",
    "età" => 30,
    "altezza" => 146,
    // ...
];
foreach ($persona as $chiave => $parametro) {
    echo $parametro;
}

$array = [
    0 => "valore 1",
    1 => "valore 2",
    "valore 3",
];
foreach ($persona as $chiave => $parametro) {
    echo $chiave . ": " . $parametro;
}

// Fa almeno una volta
do {
    // Fai qualcosa
} while ($condizione);

while ($condizione) {
    // Fai qualcosa
}

switch ($valore) {
    case "valore1":
        echo "valore 1";
        break;
    case "valore2":
        echo "valore 2";
        break;
    case "valore3":
    case "valore4":
        echo "valore 3 o 4";
        break;
    default:
        echo "altro valore";
}

$condizione = true;
$condizione2 = false;
if ($condizione) {
    // Se verificata
} elseif ($condizione2) {
    // Se verificata
} else {
   // Altrimenti
}
